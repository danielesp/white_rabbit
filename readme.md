# White Rabbit

To have the wonderful opportunity to work at Html24 as a backend developer. You have to complete this lovely little test.

* Fork this project
* Create your solution
* Make a pull request to this project


### Components
When you have completed this test succesfully, you will ~~hopefully~~ have a basic understanding of:

* git
* Docker
* PHP
* Very simple webservices
* Simple data-structures and algorithms


### The task
Upon visiting http://ip:port/task.php your program should:

* Read a file from disk
* return a sorted list of the number of occurrences of each letter in the file as json format.

We have created a skeleton for this task, but you are free to discard our files and/or add additional classes and files to it.